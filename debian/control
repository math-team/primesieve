Source: primesieve
Maintainer: Debian Math Team <team+math@tracker.debian.org>
Uploaders: Jerome Benoit <calculus@rezozer.net>
Section: math
Priority: optional
Rules-Requires-Root: no
Build-Depends:
 debhelper-compat (=13),
 cmake,
 asciidoc-base, xmlto
Build-Depends-Indep:
 doxygen, doxygen-latex, graphviz,
 rdfind, symlinks
Standards-Version: 4.7.2
Vcs-Git: https://salsa.debian.org/math-team/primesieve.git
Vcs-Browser: https://salsa.debian.org/math-team/primesieve
Homepage: https://github.com/kimwalisch/primesieve

Package: primesieve
Architecture: all
Multi-Arch: foreign
Depends: primesieve-bin, ${misc:Depends}
Description: fast prime number generator C/C++ library
 primesieve is a free software program and C/C++ library that generates
 primes using a highly optimized sieve of Eratosthenes implementation.
 primesieve can generate primes and prime k-tuplets up to nearly 2^64.
 .
 See http://primesieve.org/ for more information.
 .
 This dummy package provides the standard installation.

Package: libprimesieve12
Architecture: any
Multi-Arch: same
Section: libs
Depends: ${shlibs:Depends}, ${misc:Depends}
Recommends: primesieve-bin (= ${binary:Version})
Pre-Depends: ${misc:Pre-Depends}
Description: fast prime number generator C/C++ library -- lib
 primesieve is a free software program and C/C++ library that generates
 primes using a highly optimized sieve of Eratosthenes implementation.
 primesieve can generate primes and prime k-tuplets up to nearly 2^64.
 .
 See http://primesieve.org/ for more information.
 .
 This package provides the shared library.

Package: libprimesieve-dev
Architecture: any
Multi-Arch: same
Replaces: libprimesieve7-dev
Breaks: libprimesieve7-dev
Section: libdevel
Depends:
 libprimesieve12 (= ${binary:Version}), libprimesieve-dev-common (= ${source:Version}),
 ${misc:Depends}
Suggests: primesieve-doc (= ${source:Version}), pkgconf
Description: fast prime number generator C/C++ library -- libdev
 primesieve is a free software program and C/C++ library that generates
 primes using a highly optimized sieve of Eratosthenes implementation.
 primesieve can generate primes and prime k-tuplets up to nearly 2^64.
 .
 See http://primesieve.org/ for more information.
 .
 This package provides the static library and symbolic links needed
 for development.

Package: libprimesieve-dev-common
Architecture: all
Multi-Arch: foreign
Replaces: libprimesieve7-dev-common
Breaks: libprimesieve7-dev-common
Section: libdevel
Depends: ${misc:Depends}
Recommends: libprimesieve-dev (= ${binary:Version})
Description: fast prime number generator C/C++ library -- headers
 primesieve is a free software program and C/C++ library that generates
 primes using a highly optimized sieve of Eratosthenes implementation.
 primesieve can generate primes and prime k-tuplets up to nearly 2^64.
 .
 See http://primesieve.org/ for more information.
 .
 This package provides the headers needed by developers.

Package: primesieve-bin
Architecture: any
Multi-Arch: foreign
Depends: ${shlibs:Depends}, ${misc:Depends}
Suggests: primesieve-doc (= ${source:Version})
Description: fast prime number generator C/C++ library -- bin
 primesieve is a free software program and C/C++ library that generates
 primes using a highly optimized sieve of Eratosthenes implementation.
 primesieve can generate primes and prime k-tuplets up to nearly 2^64.
 .
 See http://primesieve.org/ for more information.
 .
 This package provides the command line utility primesieve.

Package: primesieve-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends: libjs-jquery, ${misc:Depends}
Suggests: pdf-viewer, www-browser
Enhances: primesieve-bin (= ${binary:Version}), libprimesieve-dev (= ${binary:Version})
Description: fast prime number generator C/C++ library -- doc
 primesieve is a free software program and C/C++ library that generates
 primes using a highly optimized sieve of Eratosthenes implementation.
 primesieve can generate primes and prime k-tuplets up to nearly 2^64.
 .
 See http://primesieve.org/ for more information.
 .
 This package provides the API documentation with its concomitant examples.
